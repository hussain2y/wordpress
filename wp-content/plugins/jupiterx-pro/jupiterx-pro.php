<?php
/**
 * Plugin Name: Jupiter X Pro
 * Plugin URI: https://jupiterx.com
 * Description: Jupiter X Pro
 * Version: 1.2.0
 * Author: Artbees
 * Author URI: https://artbees.net
 * Text Domain: jupiterx-pro
 * Domain Path: languages
 * License: GPL2
 *
 * @package JupiterX_Pro
 */

defined( 'ABSPATH' ) || die();

if ( ! class_exists( 'JupiterX_Pro' ) ) {

	/**
	 * Jupiter Core class.
	 *
	 * @since 1.0.0
	 */
	class JupiterX_Pro {

		/**
		 * Jupiter Core instance.
		 *
		 * @since 1.0.0
		 *
		 * @access private
		 * @var JupiterX_Pro
		 */
		private static $instance;

		/**
		 * The plugin version number.
		 *
		 * @since 1.0.0
		 *
		 * @access private
		 * @var string
		 */
		private static $version;

		/**
		 * The plugin basename.
		 *
		 * @since 1.0.0
		 *
		 * @access private
		 * @var string
		 */
		private static $plugin_basename;

		/**
		 * The plugin name.
		 *
		 * @since 1.0.0
		 *
		 * @access private
		 * @var string
		 */
		private static $plugin_name;

		/**
		 * The plugin directory.
		 *
		 * @since 1.0.0
		 *
		 * @access private
		 * @var string
		 */
		private static $plugin_dir;

		/**
		 * The plugin URL.
		 *
		 * @since 1.0.0
		 *
		 * @access private
		 * @var string
		 */
		private static $plugin_url;

		/**
		 * The plugin status.
		 *
		 * @since 1.0.0
		 *
		 * @access private
		 * @var boolean
		 */
		private $active = false;

		/**
		 * Returns JupiterX_Pro instance.
		 *
		 * @since 1.0.0
		 *
		 * @return JupiterX_Pro
		 */
		public static function get_instance() {
			if ( is_null( self::$instance ) ) {
				self::$instance = new self();
			}
			return self::$instance;
		}

		/**
		 * Returns the version number of the plugin.
		 *
		 * @since 1.0.0
		 *
		 * @return string
		 */
		public function version() {
			return self::$version;
		}

		/**
		 * Returns the plugin basename.
		 *
		 * @since 1.0.0
		 *
		 * @return string
		 */
		public function plugin_basename() {
			return self::$plugin_basename;
		}

		/**
		 * Returns the plugin name.
		 *
		 * @since 1.0.0
		 *
		 * @return string
		 */
		public function plugin_name() {
			return self::$plugin_name;
		}

		/**
		 * Returns the plugin directory.
		 *
		 * @since 1.0.0
		 *
		 * @return string
		 */
		public function plugin_dir() {
			return self::$plugin_dir;
		}

		/**
		 * Returns the plugin URL.
		 *
		 * @since 1.0.0
		 *
		 * @return string
		 */
		public function plugin_url() {
			return self::$plugin_url;
		}

		/**
		 * Constructor.
		 *
		 * @since 1.0.0
		 */
		public function __construct() {
			add_action( 'jupiterx_init', [ $this, 'load_plugin' ] );
		}

		/**
		 * Defines constants used by the plugin.
		 *
		 * @since 1.0.0
		 */
		public function define_constants() {
			$plugin_data = get_file_data( __FILE__, array( 'Plugin Name', 'Version' ), 'jupiterx-pro' );

			self::$plugin_basename = plugin_basename( __FILE__ );
			self::$plugin_name     = array_shift( $plugin_data );
			self::$version         = array_shift( $plugin_data );
			self::$plugin_dir      = trailingslashit( plugin_dir_path( __FILE__ ) );
			self::$plugin_url      = trailingslashit( plugin_dir_url( __FILE__ ) );
		}

		/**
		 * Load plugin.
		 *
		 * @since 1.0.0
		 */
		public function load_plugin() {
			$this->define_constants();

			if ( version_compare( JUPITERX_VERSION, '1.3.0', '<' ) ) {
				add_action( 'admin_notices', [ $this, 'theme_version_notice' ] );
				return;
			}

			if ( ! jupiterx_is_registered() ) {
				add_action( 'admin_notices', [ $this, 'api_key_notice' ] );
				return;
			}

			$this->init();
		}

		/**
		 * Admin theme version notice.
		 *
		 * @since 1.0.0
		 */
		public function theme_version_notice() {
			if ( current_user_can( 'manage_network_themes' ) || current_user_can( 'update_themes' ) ) {
				?>
				<div class="notice notice-error">
					<p>
						<?php esc_html_e( 'Jupiter X Pro - This plugin requires Jupiter X theme v1.3.0 and above. Please update your theme to the latest version.', 'jupiterx-pro' ); ?>
					</p>
				</div>
				<?php
			}
		}

		/**
		 * Admin API key notice.
		 *
		 * @since 1.0.0
		 */
		public function api_key_notice() {
			if ( current_user_can( 'manage_options' ) ) {
				?>
				<div class="notice notice-error">
					<p>
						<?php
						printf(
							esc_html__( 'Jupiter X Pro - Please go to %s and enter your API key to complete registration and unlock its features.', 'jupiterx-pro' ),
							'<a href="' . admin_url( '?page=jupiterx' ) . '">' . esc_html__( 'Control Panel', 'jupiterx-pro' ) . '</a>'
						);
						?>
					</p>
				</div>
				<?php
			}
		}

		/**
		 * Initializes the plugin.
		 *
		 * @since 1.0.0
		 */
		public function init() {
			$this->active = true;

			load_plugin_textdomain( 'jupiterx-pro', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

			$this->load_files( [
				'customizer/functions',
			] );

			/**
			 * Fires after all files have been loaded.
			 *
			 * @since 1.0.0
			 *
			 * @param JupiterX_Pro
			 */
			do_action( 'jupiterx_pro_init', $this );
		}

		/**
		 * Loads specified PHP files from the plugin includes directory.
		 *
		 * @since 1.0.0
		 *
		 * @param array $file_names The names of the files to be loaded in the includes directory.
		 */
		public function load_files( $file_names = array() ) {
			foreach ( $file_names as $file_name ) {
				$path = $this->plugin_dir() . 'includes/' . $file_name . '.php';

				if ( file_exists( $path ) ) {
					require_once $path;
				}
			}
		}

		/**
		 * Get plugin status.
		 *
		 * @since 1.0.0
		 *
		 * @return boolean
		 */
		public function is_active() {
			return $this->active;
		}
	}
}

/**
 * Returns the Jupiter Core application instance.
 *
 * @since 1.0.0
 *
 * @return JupiterX_Pro
 */
function jupiterx_pro() {
	return JupiterX_Pro::get_instance();
}

/**
 * Initializes the Jupiter Core application.
 *
 * @since 1.0.0
 */
jupiterx_pro();
