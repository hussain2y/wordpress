<?php
/**
 * The Jupiter Customizer component.
 *
 * @package JupiterX_Pro\Customizer
 */

add_action( 'init', 'jupiterx_pro_customizer', 5 );
/**
 * Load customizer settings.
 *
 * @since 1.1.0
 */
function jupiterx_pro_customizer() {

	// Load all the settings.
	foreach ( glob( dirname( __FILE__ ) . '/**/*.php' ) as $setting ) {
		require_once $setting;
	}
}
