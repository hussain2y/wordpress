<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

class Jet_Engine_Custom_Gallery_Tag extends Elementor\Core\DynamicTags\Data_Tag {

	public function get_name() {
		return 'jet-post-custom-gallery';
	}

	public function get_title() {
		return __( 'JetEngine Gallery', 'jet-engine' );
	}

	public function get_group() {
		return Jet_Engine_Dynamic_Tags_Module::JET_GROUP;
	}

	public function get_categories() {
		return array(
			Jet_Engine_Dynamic_Tags_Module::GALLERY_CATEGORY,
		);
	}

	protected function _register_controls() {

		$this->add_control(
			'gallery_field',
			array(
				'label'   => __( 'Field', 'jet-engine' ),
				'type'    => Elementor\Controls_Manager::SELECT,
				'options' => $this->get_meta_fields(),
			)
		);

	}

	public function get_value( array $options = array() ) {

		$meta_field = $this->get_settings( 'gallery_field' );

		if ( empty( $meta_field ) ) {
			return array();
		}

		$current_object = jet_engine()->listings->data->get_current_object();

		if ( ! $current_object ) {
			return array();
		}

		$value = jet_engine()->listings->data->get_meta( $meta_field );

		if ( is_array( $value ) ) {
			$value = $value;
		} else {
			$value = explode( ',', $value );
		}

		return array_map( function( $item ) {
			return array( 'id' => $item );
		}, $value );

	}

	private function get_meta_fields() {

		$options = array(
			'' => __( 'Select...', 'jet-engine' ),
		);

		$meta_fields = jet_engine()->listings->data->get_listing_meta_fields();

		if ( ! $meta_fields ) {
			return $options;
		}

		foreach ( $meta_fields as $field ) {
			if ( 'gallery' === $field['type'] ) {
				$options[ $field['name'] ] = $field['title'];
			}
		}

		return $options;

	}
}
