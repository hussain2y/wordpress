<?php
/**
 * The Template for displaying all single products
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

get_header( 'shop' ); ?>
 <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"> -->

   <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
<!-- <script src="//code.jquery.com/jquery-1.12.4.js"></script>
  <script src="//code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
   <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css"> -->
  <script>
$(document).ready(function(){
	/*$(".moreLinks").click(function () {
	if($('#me_second').hasClass('full_second')){
		$('#me_second').switchClass('full_second','me_second', 1000, "easeInOutQuad");
		$(".moreLinks").text('Features');
	}else{
		$('#me_second').switchClass('me_second','full_second', 1000, "easeInOutQuad");
		$(".moreLinks").text('Features less');
	}
});	*/
	jQuery('.write_review_button').click(function() {
		jQuery(".woocommerce-Reviews").toggle();
	});	
	jQuery('.moreLinks').click(function() {
		jQuery(".short-description").toggle();
		//jQuery(".short-description").addClass('display-custom');
		
	});	

});
</script>
<style>
	
.tab-content {
    color: #000;
        background: #f3f3f3;
    padding: 5px 15px;
}

 .nav-pills > li > a {
  border-radius: 0;
}
.nav-tabs .active {
    color: #666;
    background: #f3f3f3;
}
.nav-tabs li {
    cursor: pointer;
    text-align: center;
    padding: 8px 17px;
    border-radius: 0 10px 0 0;
    display: inline-block;
    font-weight: 600;
}
.nav-tabs {
    border-bottom: 0px solid #dee2e6;
}
.nav-tabs a:hover {
    text-decoration: none;
    color: #000;
}
</style>
<?php 
global $product,$WooCommerce;
 $columns           = apply_filters( 'woocommerce_product_thumbnails_columns', 4 );
$post_thumbnail_id = $product->get_image_id();
$wrapper_classes   = apply_filters( 'woocommerce_single_product_image_gallery_classes', array(
	'woocommerce-product-gallery',
	'woocommerce-product-gallery--' . ( $product->get_image_id() ? 'with-images' : 'without-images' ),
	'woocommerce-product-gallery--columns-' . absint( $columns ),
	'images',
) );
?>

<?php do_action( 'woocommerce_before_single_product' ); ?>
<div class="product type-product status-publish has-post-thumbnail first instock purchasable product-type-simple">
<div class="<?php echo esc_attr( implode( ' ', array_map( 'sanitize_html_class', $wrapper_classes ) ) ); ?>" data-columns="<?php echo esc_attr( $columns ); ?>" style="opacity: 0; transition: opacity .25s ease-in-out;">
	<figure class="woocommerce-product-gallery__wrapper left-sect">
		<?php
		if ( $product->get_image_id() ) {
			$html = wc_get_gallery_image_html( $post_thumbnail_id, true );
		} else {
			$html  = '<div class="woocommerce-product-gallery__image--placeholder">';
			$html .= sprintf( '<img src="%s" alt="%s" class="wp-post-image fotorama__img" />', esc_url( wc_placeholder_img_src( 'woocommerce_single' ) ), esc_html__( 'Awaiting product image', 'woocommerce' ) );
			$html .= '</div>';
		}
		echo apply_filters( 'woocommerce_single_product_image_thumbnail_html', $html, $post_thumbnail_id ); // phpcs:disable WordPress.XSS.EscapeOutput.OutputNotEscaped
	do_action( 'woocommerce_product_thumbnails' );
		?>
	</figure>
</div>

<div class="product-info-block right-sect">
    <div class="product-info-main">
    <div class="info-product">
    <div class="left-info">
        <?php 
        // Compatibility for WC versions from 2.5.x to 3.0+
        if ( method_exists( $product, 'get_stock_status' ) ) {
            $stock_status = $product->get_stock_status(); // For version 3.0+
        } else {
            $stock_status = $product->stock_status; // Older than version 3.0
        }
         if($stock_status=='outofstock')
         {
         	echo '<span>Out Of Stock</span>';
         }
         else
         {
         	echo '<span>In Stock</span>';
         }
    ?>
  	 <!-- <a href="#">Get delivery estimate</a> -->
    </div>
    <div class="right-info">
    <?php $product_price = new WC_Product( get_the_ID() );?>
    <h1 class="page-title"><?php echo get_the_title();?></h1>
    <span class="price"><?php echo $product_price->get_price_html();?></span>
		<form  class="cart" action="<?php echo get_the_permalink();?>" method="post" enctype="multipart/form-data">
		<button type="submit" name="add-to-cart" value="<?php echo get_the_ID();?>" class="new_inner_button">Add to cart</button>
		</form>
		
	

    <!-- <div data-amount="69900" class="extra-info"><a href="#">Retail $500 <span> - You save $241 </span><span class="af-about-small"></span></a></div> -->



    </div>
    </div>
    <div class="clearfix"></div>

  

       <div class="details-prod">
       <div class="short-desc">
       <div class="tab-section">
            <ul class="nav nav-tabs">
		    <li class="active">
            <a  href="#1a" data-toggle="tab">Description</a>
			</li>
			<li><a href="#2a" data-toggle="tab">Specifications</a>
			</li>
        </ul>
        <div class="tab-content">
	        <div class="tab-pane active" id="1a">	
	            <p class="custom-text"> 
	               <!-- <h2>Description</h2> -->
				   <?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt );?>
				 <?php echo $product_price->get_description(); ?>
				</p>
	        </div>
	         <div class="tab-pane" id="2a">
	                      	<table class="custom_table">
             	<!-- <h2>SPECIFICATIONS</h2> -->

		      		<?php if(get_post_meta(get_the_ID(),'product dimensions (w x d x h)', true) != ''){ ?>
					<tr class="custom-tr">				  	
					 	<td class="custom-key"> Product Dimensions </td>
					    <td class="custom-value">
					  <?php echo get_post_meta(get_the_ID(),'product dimensions (w x d x h)', true);?> </td>
					   </tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'net weight', true) != ''){ ?>
					 <tr class="custom-tr">	  				  	
					 	<td class="custom-key"> Net Weight</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'net weight', true);?></td>
					</tr>			
					<?php } ?>

					<?php if(get_post_meta(get_the_ID(),'product color', true) != ''){ ?>
					  <tr class="custom-tr">			  	
					 	<td class="custom-key"> Product Color</td>
					    <td class="custom-value">
					  <?php echo get_post_meta(get_the_ID(),'product color', true);?> </td>
					    </tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'product material', true) != ''){ ?>
					  <tr class="custom-tr">					  	
					 	<td class="custom-key"> Product Material</td>
					    <td class="custom-value">
					  <?php echo get_post_meta(get_the_ID(),'product material', true);?> </td>
					   </tr>
				  <?php } ?>

				   <?php if(get_post_meta(get_the_ID(),'seat width', true) != ''){ ?>
					  <tr class="custom-tr">
					  	<td class="custom-key"> Seat Width</td>
					    <td class="custom-value"><?php echo get_post_meta(get_the_ID(),'seat width', true);?></td>
					  </tr>
				  <?php } ?>

				   <?php if(get_post_meta(get_the_ID(),'seat depth', true) != ''){ ?>
					   <tr class="custom-tr">
					   	<td class="custom-key"> Seat Depth</td>
					    <td class="custom-value">
					  <?php echo get_post_meta(get_the_ID(),'seat depth', true);?> </td>
					  </tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'seat height', true) != ''){ ?>
					<tr class="custom-tr">
						<td class="custom-key"> Seat Height</td>
					    <td class="custom-value"><?php echo get_post_meta(get_the_ID(),'seat height', true);?></td>
					   </tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'arm height', true) != ''){ ?>
					  <tr class="custom-tr">
					  	<td class="custom-key"> Arm Height</td>
					    <td class="custom-value"><?php echo get_post_meta(get_the_ID(),'arm height', true);?></td>
					   </tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'leg height', true) != ''){ ?>
					  <tr class="custom-tr">			  				  	
					 	<td class="custom-key"> Leg Height</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'leg height', true);?> </td>
					</tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'leg color', true) != ''){ ?>
					  <tr class="custom-tr">			  				  	
					 	<td class="custom-key"> Leg Color</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'leg color', true);?> </td>
					</tr>
				  <?php } ?>

				   <?php if(get_post_meta(get_the_ID(),'leg material', true) != ''){ ?>
					 <tr class="custom-tr">					  				  	
					 	<td class="custom-key"> Leg Material</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'leg material', true);?>  </td>
					    </tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'weightcapacity', true) != ''){ ?>
					 <tr class="custom-tr">	  				  	
					 	<td class="custom-key">Weight Capacity</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'weightcapacity', true);?></td>
					</tr>			
					<?php } ?>

					<?php if(get_post_meta(get_the_ID(),'swatch', true) != ''){ ?>
					 <tr class="custom-tr">	  				  	
					 	<td class="custom-key">Swatch</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'swatch', true);?></td>
					</tr>			
					<?php } ?>

					</table>
	         </div>
	    </div>    
        </div>



         <?php// echo apply_filters( 'woocommerce_short_description', $post->post_excerpt );?>
         <div class="hide-mob">
         <p class="custom-text"> 
          <h2>Description</h2>
			<?php echo apply_filters( 'woocommerce_short_description', $post->post_excerpt );?>
			 <?php echo $product_price->get_description(); ?>
			</p>
          <table class="custom_table">
		    	<h2>SHIPPING</h2>
		      		<?php if(get_post_meta(get_the_ID(),'shipping info', true) != ''){ ?>
					  <tr class="custom-tr">
					  	<td class="custom-key"> Shipping Method</td>
					    <td class="custom-value">
					  <?php echo get_post_meta(get_the_ID(),'shipping info', true);?> </td>
					   </tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'carton dimensions (w x d x h)', true) != ''){ ?>
					 <tr class="custom-tr">
					 	<td class="custom-key"> Carton Dimensions (w x d x h)</td>
					    <td class="custom-value">
					  <?php echo get_post_meta(get_the_ID(),'carton dimensions (w x d x h)', true);?> </td>
					    </tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'gross weight', true) != ''){ ?>
					 <tr class="custom-tr">			  				  	
					 	<td class="custom-key"> Gross Weight</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'gross weight', true);?> </td>
					   </tr>
				  <?php }?> 

				  <?php if(get_post_meta(get_the_ID(),'pack', true) != ''){ ?>
					 <tr class="custom-tr">	  				  	
					 	<td class="custom-key"> Pack</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'pack', true);?></td>
					 </tr>			
					<?php } ?>

					 <?php if(get_post_meta(get_the_ID(),'total cartons', true) != ''){ ?>
					 <tr class="custom-tr">
					 	<td class="custom-key"> Total Cartons</td>
					    <td class="custom-value">
					  <?php echo get_post_meta(get_the_ID(),'total cartons', true);?> </td>
					   </tr>
				  <?php } ?>

					<!-- <?php if(get_post_meta(get_the_ID(),'packaging material', true) != ''){ ?>
					 <tr class="custom-tr">	  				  	
					 	<td class="custom-key"> Packaging Material</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'packaging material', true);?></td>
					</tr>			
					<?php } ?> -->

					<!--  <?php if(get_post_meta(get_the_ID(),'foam type', true) != ''){ ?>
					 <tr class="custom-tr">	  				  	
					 	<td class="custom-key"> Foam Type</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'foam type', true);?></td>
					</tr>			
					<?php } ?> -->

					<!-- <?php if(get_post_meta(get_the_ID(),'countryoforigin', true) != ''){ ?>
					 <tr class="custom-tr">	  				  	
					 	<td class="custom-key"> Country of Origin</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'countryoforigin', true);?></td>
					     </tr>			
					<?php } ?>
 -->
					<?php if(get_post_meta(get_the_ID(),'product instructions', true) != ''){ ?>
					 <tr class="custom-tr">					 				  	
					 	<td class="custom-key"> Product Instructions</td>
					    <td class="custom-value">
					 <?php echo get_post_meta(get_the_ID(),'product instructions', true);?> </td>
					   </tr>
				  <?php } ?>
		      	</table>
             	
       </div>
       </div>
       <div class="prd-img">
       <div class="hide-mob">
        <table class="custom_table">
             	<h2>SPECIFICATIONS</h2>

		      		<?php if(get_post_meta(get_the_ID(),'product dimensions (w x d x h)', true) != ''){ ?>
					<tr class="custom-tr">				  	
					 	<td class="custom-key"> Product Dimensions </td>
					    <td class="custom-value">
					  <?php echo get_post_meta(get_the_ID(),'product dimensions (w x d x h)', true);?> </td>
					   </tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'net weight', true) != ''){ ?>
					 <tr class="custom-tr">	  				  	
					 	<td class="custom-key"> Net Weight</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'net weight', true);?></td>
					</tr>			
					<?php } ?>

					<?php if(get_post_meta(get_the_ID(),'product color', true) != ''){ ?>
					  <tr class="custom-tr">			  	
					 	<td class="custom-key"> Product Color</td>
					    <td class="custom-value">
					  <?php echo get_post_meta(get_the_ID(),'product color', true);?> </td>
					    </tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'product material', true) != ''){ ?>
					  <tr class="custom-tr">					  	
					 	<td class="custom-key"> Product Material</td>
					    <td class="custom-value">
					  <?php echo get_post_meta(get_the_ID(),'product material', true);?> </td>
					   </tr>
				  <?php } ?>

				   <?php if(get_post_meta(get_the_ID(),'seat width', true) != ''){ ?>
					  <tr class="custom-tr">
					  	<td class="custom-key"> Seat Width</td>
					    <td class="custom-value"><?php echo get_post_meta(get_the_ID(),'seat width', true);?></td>
					  </tr>
				  <?php } ?>

				   <?php if(get_post_meta(get_the_ID(),'seat depth', true) != ''){ ?>
					   <tr class="custom-tr">
					   	<td class="custom-key"> Seat Depth</td>
					    <td class="custom-value">
					  <?php echo get_post_meta(get_the_ID(),'seat depth', true);?> </td>
					  </tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'seat height', true) != ''){ ?>
					<tr class="custom-tr">
						<td class="custom-key"> Seat Height</td>
					    <td class="custom-value"><?php echo get_post_meta(get_the_ID(),'seat height', true);?></td>
					   </tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'arm height', true) != ''){ ?>
					  <tr class="custom-tr">
					  	<td class="custom-key"> Arm Height</td>
					    <td class="custom-value"><?php echo get_post_meta(get_the_ID(),'arm height', true);?></td>
					   </tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'leg height', true) != ''){ ?>
					  <tr class="custom-tr">			  				  	
					 	<td class="custom-key"> Leg Height</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'leg height', true);?> </td>
					</tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'leg color', true) != ''){ ?>
					  <tr class="custom-tr">			  				  	
					 	<td class="custom-key"> Leg Color</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'leg color', true);?> </td>
					</tr>
				  <?php } ?>

				   <?php if(get_post_meta(get_the_ID(),'leg material', true) != ''){ ?>
					 <tr class="custom-tr">					  				  	
					 	<td class="custom-key"> Leg Material</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'leg material', true);?>  </td>
					    </tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'weightcapacity', true) != ''){ ?>
					 <tr class="custom-tr">	  				  	
					 	<td class="custom-key">Weight Capacity</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'weightcapacity', true);?></td>
					</tr>			
					<?php } ?>

					<?php if(get_post_meta(get_the_ID(),'swatch', true) != ''){ ?>
					 <tr class="custom-tr">	  				  	
					 	<td class="custom-key">Swatch</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'swatch', true);?></td>
					</tr>			
					<?php } ?>

					</table>
					</div>

				<table class="custom_table desk-top">
		    	     <h2 class="txt">SHIPPING</h2>
		      		<?php if(get_post_meta(get_the_ID(),'shipping info', true) != ''){ ?>
					  <tr class="custom-tr">
					  	<td class="custom-key"> Shipping Method</td>
					    <td class="custom-value">
					  <?php echo get_post_meta(get_the_ID(),'shipping info', true);?> </td>
					   </tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'carton dimensions (w x d x h)', true) != ''){ ?>
					 <tr class="custom-tr">
					 	<td class="custom-key"> Carton Dimensions (w x d x h)</td>
					    <td class="custom-value">
					  <?php echo get_post_meta(get_the_ID(),'carton dimensions (w x d x h)', true);?> </td>
					    </tr>
				  <?php } ?>

				  <?php if(get_post_meta(get_the_ID(),'gross weight', true) != ''){ ?>
					 <tr class="custom-tr">			  				  	
					 	<td class="custom-key"> Gross Weight</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'gross weight', true);?> </td>
					   </tr>
				  <?php }?> 

				  <?php if(get_post_meta(get_the_ID(),'pack', true) != ''){ ?>
					 <tr class="custom-tr">	  				  	
					 	<td class="custom-key"> Pack</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'pack', true);?></td>
					 </tr>			
					<?php } ?>

					 <?php if(get_post_meta(get_the_ID(),'total cartons', true) != ''){ ?>
					 <tr class="custom-tr">
					 	<td class="custom-key"> Total Cartons</td>
					    <td class="custom-value">
					  <?php echo get_post_meta(get_the_ID(),'total cartons', true);?> </td>
					   </tr>
				  <?php } ?>

					<?php if(get_post_meta(get_the_ID(),'packaging material', true) != ''){ ?>
					 <tr class="custom-tr">	  				  	
					 	<td class="custom-key"> Packaging Material</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'packaging material', true);?></td>
					</tr>			
					<?php } ?>

					 <?php if(get_post_meta(get_the_ID(),'foam type', true) != ''){ ?>
					 <tr class="custom-tr">	  				  	
					 	<td class="custom-key"> Foam Type</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'foam type', true);?></td>
					</tr>			
					<?php } ?>

					<?php if(get_post_meta(get_the_ID(),'countryoforigin', true) != ''){ ?>
					 <tr class="custom-tr">	  				  	
					 	<td class="custom-key"> Country of Origin</td>
					    <td class="custom-value">
					    <?php echo get_post_meta(get_the_ID(),'countryoforigin', true);?></td>
					     </tr>			
					<?php } ?>

					<?php if(get_post_meta(get_the_ID(),'product instructions', true) != ''){ ?>
					 <tr class="custom-tr">					 				  	
					 	<td class="custom-key"> Product Instructions</td>
					    <td class="custom-value">
					 <?php echo get_post_meta(get_the_ID(),'product instructions', true);?> </td>
					   </tr>
				  <?php } ?>
		      	</table>
					</div>
        </div>

       </div>
 

          <!-- <a  href="javascript:void(0)" class="moreLinks">Features</a>
       <div class="short-description" style="display:none" >
       	<span><?php //echo $product_price->get_description(); ?></span>
       </div> -->

	</div>
</div>
<div class="clearfix"></div>
<div class="related-prod">
<h3>Goes well with</h3>
<?php do_action( 'related' ); ?>
</div>
<div class="clearfix"></div>
<div class="review-block">
   <div class="write_review_button"><span>Write a review</span></div>
   <div id="reviews" class="woocommerce-Reviews" style="display: none;">
	<div id="comments">
		<h2 class="woocommerce-Reviews-title"><?php
			if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' && ( $count = $product->get_review_count() ) ) {
				/* translators: 1: reviews count 2: product name */
				printf( esc_html( _n( '%1$s review for %2$s', '%1$s reviews for %2$s', $count, 'woocommerce' ) ), esc_html( $count ), '<span>' . get_the_title() . '</span>' );
			} else {
				_e( 'Reviews', 'woocommerce' );
			}
		?></h2>
		<?php if ( have_comments() ) : ?>
			<ol class="commentlist">
				<?php wp_list_comments( apply_filters( 'woocommerce_product_review_list_args', array( 'callback' => 'woocommerce_comments' ) ) ); ?>
			</ol>
			<?php if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) :
				echo '<nav class="woocommerce-pagination">';
				paginate_comments_links( apply_filters( 'woocommerce_comment_pagination_args', array(
					'prev_text' => '&larr;',
					'next_text' => '&rarr;',
					'type'      => 'list',
				) ) );
				echo '</nav>';
			endif; ?>
		<?php else : ?>
			<p class="woocommerce-noreviews"><?php _e( 'There are no reviews yet.', 'woocommerce' ); ?></p>
		<?php endif; ?>
	</div>
	<?php if ( get_option( 'woocommerce_review_rating_verification_required' ) === 'no' || wc_customer_bought_product( '', get_current_user_id(), $product->get_id() ) ) : ?>
		<div id="review_form_wrapper">
			<div id="review_form">
				<?php
					$commenter = wp_get_current_commenter();
					$comment_form = array(
						'title_reply'          => have_comments() ? __( 'Add a review', 'woocommerce' ) : sprintf( __( 'Be the first to review &ldquo;%s&rdquo;', 'woocommerce' ), get_the_title() ),
						'title_reply_to'       => __( 'Leave a Reply to %s', 'woocommerce' ),
						'title_reply_before'   => '<span id="reply-title" class="comment-reply-title">',
						'title_reply_after'    => '</span>',
						'comment_notes_after'  => '',
						'fields'               => array(
							'author' => '<p class="comment-form-author">' . '<label for="author">' . esc_html__( 'Name', 'woocommerce' ) . '&nbsp;<span class="required">*</span></label> ' .
										'<input id="author" name="author" type="text" value="' . esc_attr( $commenter['comment_author'] ) . '" size="30" required /></p>',
							'email'  => '<p class="comment-form-email"><label for="email">' . esc_html__( 'Email', 'woocommerce' ) . '&nbsp;<span class="required">*</span></label> ' .
										'<input id="email" name="email" type="email" value="' . esc_attr( $commenter['comment_author_email'] ) . '" size="30" required /></p>',
						),
						'label_submit'  => __( 'Submit', 'woocommerce' ),
						'logged_in_as'  => '',
						'comment_field' => '',
					);
					if ( $account_page_url = wc_get_page_permalink( 'myaccount' ) ) {
						$comment_form['must_log_in'] = '<p class="must-log-in">' . sprintf( __( 'You must be <a href="%s">logged in</a> to post a review.', 'woocommerce' ), esc_url( $account_page_url ) ) . '</p>';
					}
					if ( get_option( 'woocommerce_enable_review_rating' ) === 'yes' ) {
						$comment_form['comment_field'] = '<div class="comment-form-rating"><label for="rating">' . esc_html__( 'Your rating', 'woocommerce' ) . '</label><select name="rating" id="rating" required>
							<option value="">' . esc_html__( 'Rate&hellip;', 'woocommerce' ) . '</option>
							<option value="5">' . esc_html__( 'Perfect', 'woocommerce' ) . '</option>
							<option value="4">' . esc_html__( 'Good', 'woocommerce' ) . '</option>
							<option value="3">' . esc_html__( 'Average', 'woocommerce' ) . '</option>
							<option value="2">' . esc_html__( 'Not that bad', 'woocommerce' ) . '</option>
							<option value="1">' . esc_html__( 'Very poor', 'woocommerce' ) . '</option>
						</select></div>';
					}
					$comment_form['comment_field'] .= '<p class="comment-form-comment"><label for="comment">' . esc_html__( 'Your review', 'woocommerce' ) . '&nbsp;<span class="required">*</span></label><textarea id="comment" name="comment" cols="45" rows="8" required></textarea></p>';
					comment_form( apply_filters( 'woocommerce_product_review_comment_form_args', $comment_form ) );
				?>
			</div>
		</div>
	<?php else : ?>
		<p class="woocommerce-verification-required"><?php _e( 'Only logged in customers who have purchased this product may leave a review.', 'woocommerce' ); ?></p>
	<?php endif; ?>
	<div class="clear"></div>
</div>
</div>

</div>
<?php get_footer( 'shop' );

/* Omit closing PHP tag at the end of PHP files to avoid "headers already sent" issues. */
?>

<style type="text/css">
/*.me_second {
    height: 80px;
    overflow: hidden;
}
.full_second {
    height: auto;
}*/
</style>
<script>
/*var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active");
    var panel = this.nextElementSibling;
    if (panel.style.display === "block") {
      panel.style.display = "none";
    } else {
      panel.style.display = "block";
    }
  });
}*/
/*jQuery('.accordion').click(function() {
		this.toggle();
		this.classList.toggle("active");
		//jQuery(".accordion").addClass('active');
		
	});	*/
</script>

<script type="text/javascript">
$(document).ready(function() {
	$('.slick-list li img').hover(function(){
$(this).trigger( "click" );
$('.slick-list li').removeClass('slick-current');
$(this).parents('li').addClass('slick-current');
});

$('.slick-list li img').on('touchstart',function(){
	console.log('tochstart');
$(this).trigger( "click" );
$('.slick-list li').removeClass('slick-current');
$(this).parents('li').addClass('slick-current');
});



  //toggle the component with class accordion_body
  $(".accordion_head").click(function() {
  	var test = $('.plusminus img').attr('src');
  	parts = test.split("/"),
    last_part = parts[parts.length-1];
   // alert(last_part);
  	if(last_part == 'active.jpg'){
  		 $(this).removeClass('active_custom');
  	} 
  	$('.accordion').removeClass('active_custom');
  	//$(this).removeClass('active_custom');
  	 $(this).toggleClass('active_custom');
	//this.classList.toggle("active_custom");*/
    if ($('.accordion_body').is(':visible')) {
      $(".accordion_body").slideUp(300);
      $(".plusminus").html('<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/non-active.jpg">');

      //$(this).addClass('active_custom');
    }
    if ($(this).next(".accordion_body").is(':visible')) {
    	 $(this).parents("button").removeClass('active_custom');
      $(this).next(".accordion_body").slideUp(300);
      $(this).children(".plusminus").html('<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/non-active.jpg">');
      
      //$(this).addClass('active_custom');
    } else {
    	 $(this).parents("button").removeClass('active_custom');
      $(this).next(".accordion_body").slideDown(300);
      $(this).children(".plusminus").html('<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/active.jpg">');
       $(this).parents("button").addClass('active_custom');
      //$(this).removeClass('active_custom');
    }
  });
});
</script>


