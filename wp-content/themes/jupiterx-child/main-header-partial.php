<?php
/**
 * Opening markups of main section.
 *
 * @package JupiterX\Framework\Templates\Structure
 *
 * @since   1.0.0
 */

/**
 * Fires in the main header.
 *
 * @since 1.0.0
 */
do_action( 'jupiterx_main_header' );
?>
<?php if(is_shop()){
  $page_id = woocommerce_get_page_id('shop');
  $image = get_field('shop_page_banner', $page_id );?>
 <div class="shop-banner" style="background-image: url(<?php echo $image;?>)"><!-- <h1> Shop</h1> --> </div>
 <?php } ?>
<?php

jupiterx_open_markup_e( 'jupiterx_main_content', 'div', [ 'class' => 'jupiterx-main-content' ] );

	$container = 'container';

	if ( jupiterx_get_field( 'jupiterx_content_full_width' ) ) { // phpcs:ignore
		$container = 'container-fluid';
	} // phpcs:ignore

	jupiterx_open_markup_e( 'jupiterx_fixed_wrap[_main_content]', 'div', 'class=' . $container );

		jupiterx_open_markup_e(
			'jupiterx_main_grid',
			'div',
			array(
				'class' => 'row',
			)
		);

			jupiterx_open_markup_e( 'jupiterx_primary', 'div', [ 'class' => 'jupiterx-primary ' . jupiterx_get_layout_class( 'content' ) ] );
