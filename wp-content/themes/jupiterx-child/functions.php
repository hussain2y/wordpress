<?php

// Include Jupiter X.
require_once( get_template_directory() . '/lib/init.php' );

/**
 * Enqueue assets.
 *
 * Add theme style and script to Jupiter X assets files.
 */
jupiterx_add_smart_action( 'wp_enqueue_scripts', 'jupiterx_child_enqueue_scripts', 8 );

function jupiterx_child_enqueue_scripts() {

	// Add the theme style as a fragment to have access to all the variables.
	jupiterx_compiler_add_fragment( 'jupiterx', get_stylesheet_directory_uri() . '/assets/less/style.less', 'less' );

	// Add the theme script as a fragment.
	jupiterx_compiler_add_fragment( 'jupiterx', get_stylesheet_directory_uri() . '/assets/js/script.js', 'js' );

}

/**
 * Example 1
 *
 * Modify markups and attributes.
 */
// jupiterx_add_smart_action( 'wp', 'jupiterx_setup_document' );

function jupiterx_setup_document() {

	// Header
	jupiterx_add_attribute( 'jupiterx_header', 'class', 'jupiterx-child-header' );

	// Breadcrumb
	jupiterx_remove_action( 'jupiterx_breadcrumb' );

	// Post image
	jupiterx_modify_action_hook( 'jupiterx_post_image', 'jupiterx_post_header_before_markup' );

	// Post read more
	jupiterx_replace_attribute( 'jupiterx_post_more_link', 'class' , 'btn-outline-secondary', 'btn-danger' );

	// Post related
	jupiterx_modify_action_priority( 'jupiterx_post_related', 11 );

}

/**
 * Example 2
 *
 * Modify the sub footer credit text.
 */
// jupiterx_add_smart_action( 'jupiterx_subfooter_credit_text_output', 'jupiterx_child_modify_subfooter_credit' );

function jupiterx_child_modify_subfooter_credit() { ?>

	<a href="https//jupiterx.com" target="_blank">Jupiter X Child</a> theme for <a href="http://wordpress.org" target="_blank">WordPress</a>

<?php }
add_filter('acf/settings/remove_wp_meta_box', '__return_false');

function theme_name_scripts() {
    wp_enqueue_style( 'style-name', get_stylesheet_directory_uri().'/style.css' );
    //wp_enqueue_style( 'style-name', get_stylesheet_uri() );

}
add_action( 'wp_enqueue_scripts', 'theme_name_scripts' );

add_filter( 'woocommerce_price_trim_zeros', '__return_true' );

add_action( 'woocommerce_shop_loop_item_title', 'bbloomer_custom_action', 15 );
function bbloomer_custom_action() {
   global $product;
   $last_img_id = end($product->gallery_image_ids);
   $image_url =wp_get_attachment_image_src($last_img_id, 'full'); ?>
<img src='<?php echo $image_url[0]; ?>' class="secondary-image attachment-shop-catalog wp-post-image wp-post-image--secondary fadeOutUp"/>
<?php }

add_filter( 'woocommerce_email_styles', 'patricks_woocommerce_email_styles' );
function patricks_woocommerce_email_styles( $css ) {
    $css .= "#template_header_image img {
    width: 300px!important;
}";
    return $css;
}
add_action( 'related', 'woocommerce_output_related_products');

//add action give it the name of our function to run
add_action( 'woocommerce_after_shop_loop_item_title', 'wcs_stock_text_shop_page', 25 );
//create our function
function wcs_stock_text_shop_page() {
    //returns an array with 2 items availability and class for CSS
    global $product;
    $availability = $product->get_availability();
    //check if availability in the array = string 'Out of Stock'
    //if so display on page.//if you want to display the 'in stock' messages as well just leave out this, == 'Out of stock'
    if ( $availability['availability'] == 'Out of stock') {
        echo apply_filters( 'woocommerce_stock_html', '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>', $availability['availability'] );
    }
 
}
add_filter('woocommerce_catalog_orderby', 'wc_customize_product_sorting');

function wc_customize_product_sorting($sorting_options){
    $sorting_options = array(
        'menu_order' => __( 'Sorting', 'woocommerce' ),
        'popularity' => __( 'Sort by popularity', 'woocommerce' ),
        'rating'     => __( 'Sort by average rating', 'woocommerce' ),
        'date'       => __( 'Sort by newest', 'woocommerce' ),
        'price-desc' => __( 'Sort by price: high to low', 'woocommerce' ),
    );

    return $sorting_options;
}
add_theme_support( 'woocommerce' );
add_action( 'wp_head', 'quantity_wp_head' );
function quantity_wp_head() {
if ( is_product() ) {
    ?>
<style type="text/css">.quantity, .buttons_added { width:0; height:0; display: none; visibility: hidden; }
.single_add_to_cart_button.button.alt.jupiterx-icon-shopping-cart-6 {
    display: none;
}
.variations label {
    display: none;
}
.woocommerce-variation-price {
    display: none;
}
.variations_form.cart {
    float: left;
}
#variation_price {
    font-weight: bold;
    color: #000 !important;
}
</style>
<?php
}
}
add_filter( 'woocommerce_dropdown_variation_attribute_options_args', 'dropdown_variation_attribute_options', 10, 1 );
function dropdown_variation_attribute_options( $args ){

    // For attribute "Type"
    if( 'pa_color' == $args['attribute'] )
        $args['show_option_none'] = __( 'Choose Your Color', 'woocommerce' );

    // For attribute "Sizes"
    if( 'pa_material' == $args['attribute'] )
        $args['show_option_none'] = __( 'Request A Swatch', 'woocommerce' );

    return $args;
}
// Add custom note as custom cart item data
add_filter( 'woocommerce_add_cart_item_data', 'get_custom_product_note', 30, 2 );
function get_custom_product_note( $cart_item_data, $product_id ){
    if ( isset($_GET['note']) && ! empty($_GET['note']) ) {
        $cart_item_data['custom_note'] = sanitize_text_field( $_GET['note'] );
         $cart_item_data['custom_color'] = sanitize_text_field( $_GET['color'] );
        $cart_item_data['unique_key'] = md5( microtime().rand() );
    }
    return $cart_item_data;
}


// Display note in cart and checkout pages as cart item data - Optional
add_filter( 'woocommerce_get_item_data', 'display_custom_item_data', 10, 2 );
function display_custom_item_data( $cart_item_data, $cart_item ) {
    if ( isset( $cart_item['custom_note'] ) ){
        $cart_item_data[] = array(
            'name' => "For",
            'value' =>   $cart_item['custom_note'].'&nbsp;&nbsp;<b>And</b>&nbsp;&nbsp;'.$cart_item['custom_color'],
        );
    }
    return $cart_item_data;
}

// Save and display product note in orders and email notifications (everywhere)
add_action( 'woocommerce_checkout_create_order_line_item', 'add_custom_note_order_item_meta', 20, 4 );
function add_custom_note_order_item_meta( $item, $cart_item_key, $values, $order ) {
    if ( isset( $values['custom_note'] ) ){
        $item->update_meta_data( 'For',  $values['custom_note'].'&nbsp;&nbsp;<b>And</b>&nbsp;&nbsp;'. $values['custom_color']);
    }
}
 add_action( 'woocommerce_after_shop_loop_item', 'remove_add_to_cart_buttons', 1 );

    function remove_add_to_cart_buttons() {
      if( is_product_category() || is_shop()) { 
        remove_action( 'woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart' );
      }
    }