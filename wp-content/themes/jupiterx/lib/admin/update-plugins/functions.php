<?php
/**
 * Update plugins functionality.
 *
 * @package JupiterX_Core\Updater
 *
 * @since 1.0.0
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! function_exists( 'jupiterx_plugins_update' ) ) {

	add_filter( 'pre_set_site_transient_update_plugins', 'jupiterx_plugins_update' );
	/**
	 * Check if any update is available for Jupiter X plugins.
	 * We are adding available updates to WordPress
	 *
	 * @param object $data Plugins update data.
	 *
	 * @since 1.0.0
	 */
	function jupiterx_plugins_update( $data ) {

		$plugin_manager = new JupiterX_Plugin_Updater();

		$list_of_plugins = $plugin_manager->plugins_custom_api( 0, 30, [ 'slug', 'basename', 'version', 'img_url', 'source' ] );

		if ( ! is_array( $list_of_plugins ) || count( $list_of_plugins ) < 1 ) {
			return $data;
		}

		foreach ( $list_of_plugins as $key => $plugin_info ) {

			$plugins = [
				'jupiterx-core',
				'jupiterx-pro',
				'raven',
				'revslider',
				'masterslider',
				'layerslider',
				'advanced-custom-fields-pro',
				'jet-elements',
				'jet-menu',
				'jet-popup',
				'jet-tabs',
				'jet-woo-builder',
				'jet-tricks',
				'jet-engine',
				'jet-smart-filters',
			];

			if ( ! in_array( $plugin_info['slug'], $plugins, true ) ) {
				continue;
			}

			$current_plugin_version = $plugin_manager->get_plugin_version( $plugin_info['slug'] );

			if ( ! $current_plugin_version ) {
				continue;
			}

			if ( version_compare( $current_plugin_version, $plugin_info['version'] ) === -1 ) {
				$file_path = $plugin_info['basename'];
				$update    = new stdClass();

				$update->slug        = $plugin_info['slug'];
				$update->plugin      = $file_path;
				$update->new_version = $plugin_info['version'];
				$update->package     = $plugin_info['source'];
				$update->icons['1x'] = $plugin_info['img_url'];
				$update->icons['2x'] = $plugin_info['img_url'];

				$data->response[ $file_path ] = $update;
			}
		}

		return $data;
	}
}

if ( ! function_exists( 'jupiterx_plugin_update_warning' ) ) {

	add_action( 'pre_current_active_plugins', 'jupiterx_plugin_update_warning' );
	/**
	 * Render Update conflict warning on WordPress plugin page.
	 *
	 * @since 1.5.0
	 *
	 * @return void
	 */
	function jupiterx_plugin_update_warning() {
		if ( ! function_exists( 'get_plugins' ) ) {
			require_once ABSPATH . 'wp-admin/includes/plugin.php';
		}

		$wp_updated_plugins = get_site_transient( 'update_plugins' );

		$plugins = jupiterx_get_update_plugins( false );

		foreach ( $plugins as &$plugin ) {
			$plugin = (array) $plugin;
		}

		foreach ( $plugins as $plugin ) {
			// translators: 1. Heads up title.
			$message = sprintf( __( '%1$s We have found conflicts on updating this plugin. Please resolve following issues before you continue otherwise it may cause unknown issues.', 'jupiterx' ), '<b>' . __( 'Heads up!', 'jupiterx' ) . '</b>' );

			add_action(
				'in_plugin_update_message-' . $plugin['basename'],
				function ( $plugin_data, $response ) use ( $plugin, $message, $wp_updated_plugins ) {

					if ( 'wp-repo' === $plugin['version'] ) {
						if (
							empty( $wp_updated_plugins ) &&
							empty( $wp_updated_plugins->response[ $plugin['basename'] ] )
						) {
							return;
						}

						$plugin['version'] = $wp_updated_plugins
							->response[ $plugin['basename'] ]
							->new_version;
					}

					if ( version_compare( $response->new_version, $plugin['version'] ) !== 0 ) {
						return;
					}

					$conflicts = jupiterx_get_plugin_conflicts( $plugin, get_plugins() );

					if ( empty( $conflicts['plugins'] ) && empty( $conflicts['themes'] ) ) {
						return;
					}

					ob_start();
					include 'views/html-notice-update-extensions-themes-inline.php';
					echo wp_kses_post( ob_get_clean() );
					?>
					<?php
				},
				10,
				2
			);
		}
	}
}
