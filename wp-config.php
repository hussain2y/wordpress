<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wordpress' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'sorry' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '5Y.V{?R)AUxD+TQ$E-V9j</U?>+(kT0yFx7 n6.>d=d~7G3q|2#0CQ!HxI+  =uW' );
define( 'SECURE_AUTH_KEY',  '+#2_2|<9}^Pb:Z+/!wWAPSl(g$;J&tv>:uKSG%(qw^]M^(2nQe).KNiGZrj3G0ch' );
define( 'LOGGED_IN_KEY',    'Q}vXjKZjFU5rOr>y8q(AMrjeOLB[PO>_BTLdN9 *_&5iks?{K+]QJ7Uz^F/}ibS/' );
define( 'NONCE_KEY',        ')W,RTlHlO`q>{I`)>GE7}%tYbQsi;u7+(%.AYf^r=)?[5Vz{-b4`vUE$Zd^KWt4C' );
define( 'AUTH_SALT',        'Q7W,;0K=4aUuzQxHDzxt&e/m@U`]!?UPtT6AMvZ5az%B#x,_e^z[Kv #UG$+%a=0' );
define( 'SECURE_AUTH_SALT', 'NX_k9Z/%nTM!jnQ[^HbE|M8r_K<}0ne6v:4.YZb;c+$-5m3zI~Du3}2JAM-dk{en' );
define( 'LOGGED_IN_SALT',   '@Y&.P1]$$UJI#[*=n`^q@^1Euwa[kJCAfxbunPx&>t~9mHzz#3r6q`PR^S-)&.|C' );
define( 'NONCE_SALT',       '+V0@3vE#2<l#(X&s-tp,]hAUYT1/|Oakg93n<1Ui4$Vpwe39&qObu|sw%_6fe2>N' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );
define('FS_METHOD', 'direct');

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );

